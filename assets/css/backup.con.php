<section id="movies" style="margin: 10px;">
                <div class="row secBg">
                    <div class="large-12 columns">
                        <div class="column row">
                            <div class="heading category-heading clearfix">
                                <div class="cat-head pull-left">
                                    <i class="fa fa-video-camera"></i>
                                    <h4>Film Layar Lebar</h4>
                                </div>
                                <div>
                                    <div class="navText pull-right show-for-large">
                                        <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a> <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-carousel carousel" data-auto-width="true" data-autoplay="true" data-autoplay-hover="true" data-autoplay-timeout="3000" data-car-length="5" data-dots="false" data-items="6" data-loop="true" data-margin="10" id="owl-demo-movie">
                            
                            <div class="item-movie item thumb-border">

                                <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                                    <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                                    <p style="color:white !important; margin-top:19px; margin-left:90px"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:12px;">Rp 105.000</span></p>
                                </div>
                                <figure class="premium-img">
                                    <img alt="carousel" src="<?php echo base_url('assets/') ?>images/movie1.png"> <a class="hover-posts" href="single-video-v3.html">
                                    <div class="boxed">
                                        <span></span>
                                        <h3 style="color:white;"><span>Jermal</span></h3><span><em>88 Menit</em><br>
                                        Ravi Bharwani<br>
                                        <em>2008</em></span>
                                    </div></a>
                                </figure>
                            </div>
                        </div><!-- end carousel -->
                    </div>
                </div>
            </section>