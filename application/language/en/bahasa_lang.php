<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['home_tf'] 		= 'WATCH FILM'; 
$lang['home_tf_desk']	= 'Tonton film pendek Indonesia, dokumenter, trailer, video musik, iklan dan film layar lebar.';
$lang['home_fllt'] 		= 'Film Layar Lebar Terbaru';
$lang['home_vt']		= 'NEW VIDEO';
$lang['home_fi']		= 'FILM INFO';
$lang['home_fi_desk']	= 'Temukan informasi tentang film Indonesia, baik itu fiksi, pendek atau dokumenter.';
$lang['home_tdib']		= 'TAYANG DI BIOSKOP MINGGU INI';
$lang['home_fa']		= 'FILM ARCHIVE';
$lang['home_fa_desk']	= 'Watch archive materials about Indonesia from 1912 to 1965.';
$lang['home_vp']		= 'Video Pilihan';
$lang['home_fn']		= 'FILM NEWS';
$lang['home_fn_desk']	= 'Berita terbaru bioskop Indonesia dan lihat acara / festival yang akan datang';
$lang['home_bt']		= 'NEW NEWS';
$lang['tentang_kami']	= 'ABOUT US';
$lang['ikuti']			= 'FOLLOW ME';
$lang['login']			= 'LOGIN';
$lang['register']		= 'REGISTER';
$lang['bahasa']			= 'LANGUAGE';
$lang['sosial']		= 'Social Media';
$lang['cari']		= 'Search';
$lang['tonton_video']		= 'Watch Video';
$lang['menit']	= 'Minute';
$lang['selengkapnya']	= 'More';
