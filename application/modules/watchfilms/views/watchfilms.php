<section id="randomMedia2" class="search-section text-hider">
    <div class="row section-inner">
        <div class="large-12 columns">
            <div class="text-center search-container flex-all-center">
                <form action="">
                    <input type="search" placeholder="Pencarian">
                    <button type="button" class="search-button">
                        <div class="button-wrapper search-button-wrapper">
                            <span class="icon"><i class="fa fa-search"></i></span>
                        </div>
                    </button>
                    <button type="button" class="filter-button">
                        <div class="button-wrapper filter-button-wrapper">
                            <span>Filter Pencarian</span>
                            <span class="icon"><i class="fa fa-sliders"></i></span>
                        </div>
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>

<section id="movie-list-section">
    <div class="row section-inner">
        <div class="large-12 columns">
            <div class="movie-list-container">
                <div class="movie-category">
                    <div class="title">
                        <div class="title-text"> 
                            <span class="text-bold">FILM</span> <br> 
                            LAYAR LEBAR
                        </div>
                    </div>
                    <div class="button-wrapper">
                        <div class="button-more"><span>Selengkapnya</span> <i class="fa fa-chevron-right"></i></div>
                    </div>
                </div>
                <div class="movie-lists">
                    <div class="owl-carousel-movie">
                        <div class="movie-list">
                            <img src="http://116.206.196.146/uploads/2018-08/hidup-untuk-mati.jpg" alt="">
                            <div class="movie-label">Rp.15.000</div>
                        </div>
                        <div class="movie-list">
                            <img src="http://116.206.196.146/uploads/2018-08/hidup-untuk-mati.jpg" alt="">
                            <div class="movie-label">Rp.15.000</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>