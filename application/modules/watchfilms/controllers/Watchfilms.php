<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Watchfilms extends CI_Controller {
  	public function __construct()
	{
		parent::__construct();
		$this->data['module']  = 'watchfilms';
		$this->data['page'] = 'watchfilms';
		if (@$this->input->cookie('lang_is')) {
			$this->data['lang'] =  $this->input->cookie('lang_is');
		} else {
			$this->data['lang'] =  'id';
		}
		$this->load->language('bahasa',$this->data['lang']);

	}
	public function index()
	{
		$this->data['css'] = '';
	  	$this->data['js']  = '';
		$this->data['watchfilms'] = true;

	    $this->load->view('template/view_depan',$this->data);	
	}

}

/* End of file Watchfilms.php */
/* Location: ./application/modules/watchfilms/controllers/Watchfilms.php */