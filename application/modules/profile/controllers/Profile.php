<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
  	public function __construct()
	{
		parent::__construct();
		$this->data['module']  = 'profile';
		if (@$this->input->cookie('lang_is')) {
			$this->data['lang'] =  $this->input->cookie('lang_is');
		} else {
			$this->data['lang'] =  'id';
		}
		$this->load->language('bahasa',$this->data['lang']);

	}
	public function index()
	{
		
	}

}

/* End of file Profile.php */
/* Location: ./application/modules/profile/controllers/Profile.php */