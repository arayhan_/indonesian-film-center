<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {
  	public function __construct()
	{
		parent::__construct();
		$this->data['module']  = 'detail';
		$this->data['page'] = 'detail';
		if (@$this->input->cookie('lang_is')) {
			$this->data['lang'] =  $this->input->cookie('lang_is');
		} else {
			$this->data['lang'] =  'id';
		}
		$this->load->language('bahasa',$this->data['lang']);

	}
	public function index()
	{
		$this->data['css'] = '';
	  	$this->data['js']  = '';
	    $this->data['detail'] = true;

	    $this->load->view('template/view_depan',$this->data);
	}

}

/* End of file Detail.php */
/* Location: ./application/modules/detail/controllers/Detail.php */