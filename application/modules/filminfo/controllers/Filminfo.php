<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filminfo extends CI_Controller {
  	public function __construct()
	{
		parent::__construct();
		$this->data['module']  = 'filminfo';
		if (@$this->input->cookie('lang_is')) {
			$this->data['lang'] =  $this->input->cookie('lang_is');
		} else {
			$this->data['lang'] =  'id';
		}
		$this->load->language('bahasa',$this->data['lang']);

	}
	public function index()
	{
		
	}

}

/* End of file Filminfo.php */
/* Location: ./application/modules/filminfo/controllers/Filminfo.php */