</section> <div style="background: white;"><section style='background:green; border:solid 1px green;' id="randomMedia2" class="text-hider">
    <div class="row" style="background:white; margin-top:-12px; padding-top:10px; ">
        <div class="large-2  columns">
            <div class="open-heading text-center">
                <h1 style="font-size:26px;">TONTON FILM</h1>
                
            </div>
        </div>

        <div class="large-10 l columns">
            <div class="open-heading text-center">
            </br>
                <h3 style="font-size:20px; margin-top:-20px;">Tonton film pendek Indonesia, dokumenter, trailer, video musik, iklan dan film layar lebar.</h3>
            </div>
        </div>
    </div>

</section>

<section style='background:green; border:solid 1px green;' id="movies" style="margin-top: 10px;">
    <div class="row" style="background:white; margin-top:-12px;">
        <div class="large-2  columns">
            <div class="open-heading text-center">
                <h2 style="margin-top: 20px; font-size:20px;">Film Layar Lebar Terbaru</h2>
                
            </div>
        </div>

        <div class="large-10 columns">
            <div class="column row">
                <div class="heading  clearfix">
                    
                <div>
                    <div class="navText pull-right show-for-large margin-10">
                        <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a>
                        <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                </div>
            </div>
            <!-- movie carousel -->
            <div id="owl-demo-movie" class="owl-carousel carousel" data-autoplay="true" data-autoplay-timeout="3000" data-autoplay-hover="true" data-car-length="5" data-items="6" data-dots="false" data-loop="true" data-auto-width="true" data-margin="10">
                

                <!-- END  -->
                <div class="item-movie item thumb-border">

                  <a  class="btn" href="http://www.indonesianfilmcenter.com/new_web/detail/thumb/3588" rel="modal:open">
                        <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:19px; margin-left:20px"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:12px;">Rp. 15.000</span></p>
                    </div>
                  </a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/filmdata/images/cover/1280911954_POSTER-JERMAL-S_thumb.jpg"> 
                        <a class="hover-posts" href="#">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Jermal                                </h3><em> 88 Menit</em><br>
                            Ravi Bharwani<br>
                            <em>2008</em></span>
                        </div></a>
                    </figure>
                </div>  
                   <!-- END  -->
                <div class="item-movie item thumb-border">

                  <a  class="btn" href="http://www.indonesianfilmcenter.com/new_web/detail/thumb/3466" rel="modal:open">
                        <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:19px; margin-left:20px"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:12px;">Rp. 15.000</span></p>
                    </div>
                  </a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/filmdata/images/cover/cover_impiankemarau_still_thumb.jpg"> 
                        <a class="hover-posts" href="#">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Impian Kemarau                                </h3><em> 83 Menit</em><br>
                            <br>
                            <em>2004</em></span>
                        </div></a>
                    </figure>
                </div>  
                   <!-- END  -->
                <div class="item-movie item thumb-border">

                  <a  class="btn" href="http://www.indonesianfilmcenter.com/new_web/detail/thumb/149" rel="modal:open">
                        <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:19px; margin-left:20px"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:12px;">Rp. 15.000</span></p>
                    </div>
                  </a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/filmdata/images/cover/coverIdFC_03082016_80149_thumb.jpg"> 
                        <a class="hover-posts" href="#">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Pantja-Sila: Cita-cita & Realita                                </h3><em> 80 Menit</em><br>
                            <br>
                            <em>2016</em></span>
                        </div></a>
                    </figure>
                </div>  
                   <!-- END  -->
                <div class="item-movie item thumb-border">

                  <a  class="btn" href="http://www.indonesianfilmcenter.com/new_web/detail/thumb/487" rel="modal:open">
                        <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:19px; margin-left:20px"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:12px;">Rp. 15.000</span></p>
                    </div>
                  </a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/filmdata/images/cover/coverIdFC_19082013_90103_thumb.jpg"> 
                        <a class="hover-posts" href="#">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Setelah 15 Tahun                                </h3><em> 93 Menit</em><br>
                            <br>
                            <em>2013</em></span>
                        </div></a>
                    </figure>
                </div>  
                   <!-- END  -->
                <div class="item-movie item thumb-border">

                  <a  class="btn" href="http://www.indonesianfilmcenter.com/new_web/detail/thumb/13624" rel="modal:open">
                        <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:19px; margin-left:20px"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:12px;">Rp. 15.000</span></p>
                    </div>
                  </a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/uploads/2018-08/tragedi-jakarta-1998-gerakan-mahasiswa-di-indonesia.jpg"> 
                        <a class="hover-posts" href="#">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Tragedi Jakarta 1998: Gerakan Mahasiswa di Indonesia                                </h3><em> 43 Menit</em><br>
                            <br>
                            <em>2002</em></span>
                        </div></a>
                    </figure>
                </div>  
                   <!-- END  -->
                <div class="item-movie item thumb-border">

                  <a  class="btn" href="http://www.indonesianfilmcenter.com/new_web/detail/thumb/13625" rel="modal:open">
                        <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:19px; margin-left:20px"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:12px;">Rp. 15.000</span></p>
                    </div>
                  </a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/uploads/2018-08/hidup-untuk-mati.jpg"> 
                        <a class="hover-posts" href="#">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Hidup Untuk Mati                                </h3><em> 66 Menit</em><br>
                            <br>
                            <em>2011</em></span>
                        </div></a>
                    </figure>
                </div>  
                 



            </div><!-- end carousel -->
        </div>
    </div>
</section>


<section style='background:green; border:solid 1px green;' id="movies">
    <div class="row" style="background:white; margin-top:-12px;">
        <div class="large-2  columns">
            <div class="open-heading text-center">
                <h2 style="margin-top: 20px; font-size:20px;">Video Terbaru</h2>
                
            </div>
        </div>
        <div class="large-10 columns">
            <div class="column row">
                <div class="heading clearfix">
                   
                <div>
                    <div class="navText pull-right show-for-large">
                        <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a>
                        <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                </div>
            </div>
            <!-- movie carousel -->
            <div id="owl-demo-movie" class="owl-carousel carousel" data-autoplay="true" data-autoplay-timeout="3000" data-autoplay-hover="true" data-car-length="5" data-items="6" data-dots="false" data-loop="true" data-auto-width="true" data-margin="10">
               

                
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/INTERLOKAL_13701.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13701" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13701" rel="modal:open">
                        <b>INTERLOKAL </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/3 DARA 2 (02:00)_13699.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13699" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13699" rel="modal:open">
                        <b>3 DARA 2 (02:00) </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/UNBEATABLE - JFlow Feat. Dira Sugandi - Official Song Asian Games 2018_13698.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13698" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13698" rel="modal:open">
                        <b>UNBEATABLE - JFlow Feat. Dira Sugandi - Official Song Asian Games 2018 </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/BCL & JFlow - Dance Tonight_13697.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13697" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13697" rel="modal:open">
                        <b>BCL & JFlow - Dance Tonight </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/Lapar Together Funt4stic Box Forever_13696.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13696" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13696" rel="modal:open">
                        <b>Lapar Together Funt4stic Box Forever </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/Tropicana Slim Stevia - Alami dari daun Stevia_13695.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13695" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13695" rel="modal:open">
                        <b>Tropicana Slim Stevia - Alami dari daun Stevia </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/AKU INDONESIA_13694.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13694" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13694" rel="modal:open">
                        <b>AKU INDONESIA </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/Introspeksi_13690.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13690" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13690" rel="modal:open">
                        <b>Introspeksi </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/AGNEZ MO - Overdose (ft. Chris Brown)_13688.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13688" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13688" rel="modal:open">
                        <b>AGNEZ MO - Overdose (ft. Chris Brown) </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/dear-nathan-poster-trailer.png" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13685" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13685" rel="modal:open">
                        <b>Dear Nathan: Hello Salma (00:58) </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/MENUNGGU PAGI (00:59)_13681.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13681" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13681" rel="modal:open">
                        <b>MENUNGGU PAGI (00:59) </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/jjablay-poster.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13679" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13679" rel="modal:open">
                        <b>Jakarta Jablay  </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/Jakarta Jablay (00:30)_13677.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13677" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13677" rel="modal:open">
                        <b>Jakarta Jablay (00:30) </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/KESEMPATAN KEDUDA (02:00)_13676.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13676" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13676" rel="modal:open">
                        <b>KESEMPATAN KEDUDA (02:00) </a></b></h6>
                   
                </div>
            </div>
               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/nestum.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13675" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13675" rel="modal:open">
                        <b>Nestlé NESTUM. Bubur Sereal Sarapan Multigrain dengan Grainsmarta. </a></b></h6>
                   
                </div>
            </div>
            
            </div><!-- end carousel -->
        </div>
    </div>
</section>





<section style='background:green; border:solid 1px green;' id="randomMedia2" class="text-hider">
    <div class="row" style="background:white; margin-top:-12px;">
        <div class="large-2  columns">
            <div class="open-heading text-center">
                <h1 style="font-size:26px;">FILM INFO</h1>
                
            </div>
        </div>

        <div class="large-10 l columns">
            <div class="open-heading text-center">
            </br>
                <h3 style="font-size:20px; margin-top:-20px;">Temukan informasi tentang film Indonesia, baik itu fiksi, pendek atau dokumenter.</h3>
            </div>
        </div>
    </div>

</section>

<section style='background:green; border:solid 1px green;' id="movies">
    <div class="row" style="background:white; margin-top:-12px;">
         <div class="large-2  columns">
            <div class="open-heading text-center">
                <h2 style="margin-top: 20px; font-size:20px;">TAYANG DI BIOSKOP MINGGU INI</h2>
                
            </div>
        </div>
        <div class="large-10 columns">
            <div class="column row">
                <div class="heading clearfix">
                   
                <div>
                    <div class="navText pull-right show-for-large">
                        <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a>
                        <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                </div>
            </div>
            <!-- movie carousel -->
            <div id="owl-demo-movie" class="owl-carousel carousel" data-autoplay="true" data-autoplay-timeout="3000" data-autoplay-hover="true" data-car-length="5" data-items="6" data-dots="false" data-loop="true" data-auto-width="true" data-margin="10">
               

               
                              <div class="item-movie item thumb-border">
                    <a  href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13669" rel="modal:open">
                    <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:20px; margin-left:0px; margin-right: 7px;"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:18px;">NEW</span></p>
                    </div></a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/uploads/2018-09/siap-gan-poster.jpg"> <a class="hover-posts" href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13669" rel="modal:open">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Siap Gan!                                </h3><em> 93 Menit</em><br>
                            
                            <em>2018</em></span>
                        </div></a>
                    </figure>
                </div>  
              
              
              
                              <div class="item-movie item thumb-border">
                    <a  href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13627" rel="modal:open">
                    <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:20px; margin-left:0px; margin-right: 7px;"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:18px;">NEW</span></p>
                    </div></a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/uploads/2018-08/gila-lu-ndro.jpg"> <a class="hover-posts" href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13627" rel="modal:open">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Gila Lu Ndro                                </h3><em> 79 Menit</em><br>
                            
                            <em>2018</em></span>
                        </div></a>
                    </figure>
                </div>  
              
              
              
                              <div class="item-movie item thumb-border">
                    <a  href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13595" rel="modal:open">
                    <div class="watermark" style="background:url('http://www.indonesianfilmcenter.com/images/label-harga.png');width:99%;height:100%;background-position:top right;background-repeat:no-repeat;">
                        <span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center;font-size:12px;"><!--kerjaan bayu--></span>
                        <p style="color:white !important; margin-top:20px; margin-left:0px; margin-right: 7px;"><span class="pull-right" style="transform: rotate(48deg) ; -webkit-transform: rotate(48deg) ; -moz-transform: rotate(48deg) ; -o-transform: rotate(48deg) ; -ms-transform: rotate(48deg) ; color:white; text-align:center; font-size:18px;">NEW</span></p>
                    </div></a>
                    <figure class="premium-img">
                        <img alt="carousel" src="http://116.206.196.146/uploads/2018-08/bisikan-iblis-poster.jpg"> <a class="hover-posts" href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13595" rel="modal:open">
                        <div class="boxed">
                            
                            <span>
                                <h3 style="color:white; font-size:14px;">
                                Bisikan Iblis                                </h3><em> 87 Menit</em><br>
                            
                            <em>2018</em></span>
                        </div></a>
                    </figure>
                </div>  
              
              
              
                          <div class="item-movie item thumb-border">
                    <figure class="premium-img">
                        <img src="http://116.206.196.146/uploads/2018-08/udah-putusin-aja-poster.jpg" alt="carousel">
                        <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13070" rel="modal:open" class="hover-posts">
                            <div class="boxed">
                
                            <span><h3 style="color:white; font-size:14px;">
                                Udah Putusin Aja!                            </h3><em> 88 Menit</em><br>
                            
                            <em>2017</em></span>
                        </div>
                        </a>
                    </figure>
                </div>
              
              
              
                          <div class="item-movie item thumb-border">
                    <figure class="premium-img">
                        <img src="http://116.206.196.146/uploads/2018-08/jejak-cinta.jpg" alt="carousel">
                        <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13585" rel="modal:open" class="hover-posts">
                            <div class="boxed">
                
                            <span><h3 style="color:white; font-size:14px;">
                                Jejak Cinta                            </h3><em> 100 Menit</em><br>
                            
                            <em>2018</em></span>
                        </div>
                        </a>
                    </figure>
                </div>
              
              
              
                          <div class="item-movie item thumb-border">
                    <figure class="premium-img">
                        <img src="http://116.206.196.146/uploads/2018-07/wiro-sableng-pendekar-kapak-maut-naga-geni-212.jpg" alt="carousel">
                        <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13496" rel="modal:open" class="hover-posts">
                            <div class="boxed">
                
                            <span><h3 style="color:white; font-size:14px;">
                                Wiro Sableng: Pendekar Kapak Maut Naga Geni 212                            </h3><em> 123 Menit</em><br>
                            
                            <em>2018</em></span>
                        </div>
                        </a>
                    </figure>
                </div>
              
              
              
                          <div class="item-movie item thumb-border">
                    <figure class="premium-img">
                        <img src="http://116.206.196.146/uploads/2018-08/sultan-agung-tahta-perjuangan-cinta-poster.jpg" alt="carousel">
                        <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13549" rel="modal:open" class="hover-posts">
                            <div class="boxed">
                
                            <span><h3 style="color:white; font-size:14px;">
                                Sultan Agung: Tahta, Perjuangan, Cinta                            </h3><em> 148 Menit</em><br>
                            
                            <em>2018</em></span>
                        </div>
                        </a>
                    </figure>
                </div>
              
              
              
                          <div class="item-movie item thumb-border">
                    <figure class="premium-img">
                        <img src="http://116.206.196.146/uploads/2018-07/cinta-sama-dengan-cindolo-na-tape.jpg" alt="carousel">
                        <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13381" rel="modal:open" class="hover-posts">
                            <div class="boxed">
                
                            <span><h3 style="color:white; font-size:14px;">
                                Cinta sama dengan Cindolo na Tape                            </h3><em> 107 Menit</em><br>
                            
                            <em>2018</em></span>
                        </div>
                        </a>
                    </figure>
                </div>
              
              
              
                          <div class="item-movie item thumb-border">
                    <figure class="premium-img">
                        <img src="http://116.206.196.146/uploads/2018-07/rompis.jpg" alt="carousel">
                        <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13403" rel="modal:open" class="hover-posts">
                            <div class="boxed">
                
                            <span><h3 style="color:white; font-size:14px;">
                                Rompis                            </h3><em> 99 Menit</em><br>
                            
                            <em>2018</em></span>
                        </div>
                        </a>
                    </figure>
                </div>
              
              
            
            </div><!-- end carousel -->
        </div>
    </div>
</section>

<section style='background:green; border:solid 1px green;' id="randomMedia2" class="text-hider">
    <div class="row" style="background:white; margin-top:-12px;">
        <div class="large-2  columns">
            <div class="open-heading text-center">
                <h1 style="font-size:26px;">FILM ARCHIVE</h1>
                
            </div>
        </div>

        <div class="large-10 l columns">
             <h3 style="font-size:20px;">Watch archive materials about Indonesia from 1912 to 1965.</h3>
        </div>
    </div>

</section>
<section style='background:green; border:solid 1px green;' id="movies">
    <div class="row" style="background:white; margin-top:-12px;">
         <div class="large-2  columns">
            <div class="open-heading text-center">
                <h2 style="margin-top: 20px; font-size:20px;">Video Pilihan</h2>
                
            </div>
        </div>
        <div class="large-10 columns">
            <div class="column row">
                <div class="heading clearfix">
                   
                <div>
                    <div class="navText pull-right show-for-large">
                        <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a>
                        <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                </div>
            </div>
            <!-- movie carousel -->
            <div id="owl-demo-movie" class="owl-carousel carousel" data-autoplay="true" data-autoplay-timeout="3000" data-autoplay-hover="true" data-car-length="5" data-items="6" data-dots="false" data-loop="true" data-auto-width="true" data-margin="10">
               

               
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/1970s.png" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13254" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13254" rel="modal:open">
                        <b>1970s NG steam 1 - Sumatra 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/Sea passages.png" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13253" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13253" rel="modal:open">
                        <b>Sea passages - 1974 - to Indonesia around the Cape of Good Hope 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/jakarta1941.png" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13240" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13240" rel="modal:open">
                        <b>Jakarta 1941 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/auto_thumb/stasiun-gambir.png" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13181" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/13181" rel="modal:open">
                        <b>Stasiun Gambir 1986 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_11042011_135432.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10761" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10761" rel="modal:open">
                        <b>KEDATANGAN DARI MANAJER UMUM NSB, IR. A.A. MUSSERT DI BATAVIA 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_15032011_163600.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10759" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10759" rel="modal:open">
                        <b>BERITA ANIF - KUNJUNGAN RESMI H.R.H. SULTAN DJOKJAKARTA KE GUBERNUR 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_15032011_164731.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10756" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10756" rel="modal:open">
                        <b>BERITA ANIF- ULANG TAHUN 12 1/2 TAHUN KETUA PRAMUKA DARI KLUB PRAMUKA BELANDA 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_05042011_180546.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10754" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10754" rel="modal:open">
                        <b>BALI (BABAK 1) 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_15032011_174009.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10744" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10744" rel="modal:open">
                        <b>BALI (BABAK 2) 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_15032011_193903.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10732" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10732" rel="modal:open">
                        <b>LEPROZERIE LAO SIMOMO. GAMBAR JALANAN DJOKJAKARTA 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_16032011_130641.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10731" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10731" rel="modal:open">
                        <b>PERESMIAN RADIO-SAMBUNGAN TELEPON BELANDA-HINDIA BELANDA 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_16032011_132918.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10730" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10730" rel="modal:open">
                        <b>UPACARA PENGUBURAN JENDRAL VAN HEUTSZ 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_16032011_133720.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10729" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10729" rel="modal:open">
                        <b>PENERBANGAN PENUMPANG PERTAMA AMSTERDAM-BATAVIA 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_16032011_165731.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10728" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10728" rel="modal:open">
                        <b>KEBERANGKATAN PERTAMA -K.N.I.L.M.- PESAWAT KE HINDIA BELANDA 
                             </a></b></h6>
                   
                </div>
            </div>
              
              <div class="video-box thumb-border" style="width: 230px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/filmdata/images/thumb/smallthumb_vidIdFC_16032011_173952.jpg" alt="most viewed videos">
                    <a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10727" rel="modal:open" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6><a href="http://www.indonesianfilmcenter.com/new_web/detail/thumbs/10727" rel="modal:open">
                        <b>PEMBUKAAN RESMI I.T.A. PAMERAN HINDIA BELANDA DI ARNHEM 
                             </a></b></h6>
                   
                </div>
            </div>
            
            </div><!-- end carousel -->
        </div>
    </div>
</section>


<section style='background:green; border:solid 1px green;' id="randomMedia2" class="text-hider">
    <div class="row" style="background:white; margin-top:-12px;">
        <div class="large-2  columns">
            <div class="open-heading text-center">
                <h1 style="font-size:26px;">FILM NEWS</h1>
                
            </div>
        </div>

        <div class="large-10 l columns">
           <div class="open-heading text-center">
            </br>
                <h3 style="font-size:20px; margin-top:-20px;">Berita terbaru bioskop Indonesia dan lihat acara / festival yang akan datang</h3>
            </div>
        </div>
    </div>

</section>

<section style='background:green; border:solid 1px green;' id="movies">
    <div class="row" style="background:white; margin-top:-12px;">
         <div class="large-2  columns">
            <div class="open-heading text-center">
                <h2 style="margin-top: 20px; font-size:20px;">BERITA TERBARU</h2>
                
            </div>
        </div>
        <div class="large-10 columns">

            <div class="column row">
                <div class="heading clearfix">
                   
                <div>
                    <div class="navText pull-right show-for-large">
                        <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a>
                        <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                </div>
            </div>
            <!-- movie carousel -->
            <div id="owl-demo-movie" class="owl-carousel carousel" data-autoplay="true" data-autoplay-timeout="3000" data-autoplay-hover="true" data-car-length="5" data-items="6" data-dots="false" data-loop="true" data-auto-width="true" data-margin="10">
               

                
            <div class="video-box thumb-border" style="width: 235px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/1867615605.jpg" alt="most viewed videos">
                  
                    <a href="http://www.indonesianfilmcenter.com/new_web/filmnews" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6 style="margin-top: -10px;">
                        <b> <a href='https://entertainment.kompas.com' target='_blank' style='text-decoration:underline;color:#007eff;'>entertainment.kompas.com</a>                                   </b>
                               </h6>
                               <h3> <p><span style="font-size: 18px; line-height: 115%; font-family: 'arial black', 'avant garde'; color: black; background-repeat: initial; background-color: white;"><a href="http://www.indonesianfilmcenter.com/detail/index/marlina-si-pembunuh-dalam-empat-babak/7" target="_blank" rel="noopener">Marlina si Pembunuh dalam Empat Babak</a> Wakili Indonesia di Seleksi Oscar 2019</span></p></h3>
                               <small >19 September 2018</small>
                   
                </div>
            </div>
               
            <div class="video-box thumb-border" style="width: 235px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/wfafafvav.png" alt="most viewed videos">
                  
                    <a href="http://www.indonesianfilmcenter.com/new_web/filmnews" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6 style="margin-top: -10px;">
                        <b> <a href='https://sidomi.com' target='_blank' style='text-decoration:underline;color:#007eff;'>sidomi.com</a>                                   </b>
                               </h6>
                               <h3> <p><span style="font-size: 18px; line-height: 115%; font-family: 'arial black', 'avant garde'; color: black; background-repeat: initial; background-color: white;"><a href="http://www.indonesianfilmcenter.com/detail/index/generasi-micin/13691" target="_blank" rel="noopener">Generasi Micin</a> Siap Viral Di Bioskop 18 Oktober</span></p></h3>
                               <small >18 September 2018</small>
                   
                </div>
            </div>
               
            <div class="video-box thumb-border" style="width: 235px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/ojojsdklj.jpg" alt="most viewed videos">
                  
                    <a href="http://www.indonesianfilmcenter.com/new_web/filmnews" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6 style="margin-top: -10px;">
                        <b> <a href='https://celebrity.okezone.com' target='_blank' style='text-decoration:underline;color:#007eff;'>celebrity.okezone.com</a>                                   </b>
                               </h6>
                               <h3> <p><span style="font-size: 18px; line-height: 115%; font-family: 'arial black', 'avant garde'; color: black; background-repeat: initial; background-color: white;">Ketika BEKRAF Jadi Mak Comblang di Industri Film Indonesia</span></p></h3>
                               <small >18 September 2018</small>
                   
                </div>
            </div>
               
            <div class="video-box thumb-border" style="width: 235px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/agsegsdg.jpg" alt="most viewed videos">
                  
                    <a href="http://www.indonesianfilmcenter.com/new_web/filmnews" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6 style="margin-top: -10px;">
                        <b> <a href='https://www.viva.co.id' target='_blank' style='text-decoration:underline;color:#007eff;'>www.viva.co.id</a>                                   </b>
                               </h6>
                               <h3> <p><span style="font-size: 18px; line-height: 115%; font-family: 'arial black', 'avant garde'; color: black; background-repeat: initial; background-color: white;">Keluar dari Zona Nyaman, <a href="http://www.indonesianfilmcenter.com/profil/index/director/9193" target="_blank" rel="noopener">Aura Kasih</a> Terjun ke Film Horor</span></p></h3>
                               <small >18 September 2018</small>
                   
                </div>
            </div>
               
            <div class="video-box thumb-border" style="width: 235px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/fdnmnfgbf.jpeg" alt="most viewed videos">
                  
                    <a href="http://www.indonesianfilmcenter.com/new_web/filmnews" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6 style="margin-top: -10px;">
                        <b> <a href='https://www.liputan6.com' target='_blank' style='text-decoration:underline;color:#007eff;'>www.liputan6.com</a>                                   </b>
                               </h6>
                               <h3> <p><span style="font-size: 18px; line-height: 115%; font-family: 'arial black', 'avant garde'; color: black; background: white;">Bukan Biopik, Film The Sacred Riana Bakal Berjenis Horor Fantasi</span></p></h3>
                               <small >18 September 2018</small>
                   
                </div>
            </div>
               
            <div class="video-box thumb-border" style="width: 235px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/jgfjghnd.jpg" alt="most viewed videos">
                  
                    <a href="http://www.indonesianfilmcenter.com/new_web/filmnews" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6 style="margin-top: -10px;">
                        <b> <a href='https://wowkeren.com' target='_blank' style='text-decoration:underline;color:#007eff;'>wowkeren.com</a>                                   </b>
                               </h6>
                               <h3> <p><span style="font-size: 18px; line-height: 115%; font-family: 'arial black', 'avant garde'; color: black; background-repeat: initial; background-color: white;"><a href="http://www.indonesianfilmcenter.com/profil/index/director/15385" target="_blank" rel="noopener">Laura Theux</a> Perankan Taruni Akpol Di Film Terbaru 'Pohon Terkenal'</span></p></h3>
                               <small >17 September 2018</small>
                   
                </div>
            </div>
               
            <div class="video-box thumb-border" style="width: 235px;">
                <div class="video-img-thumb">
                    <img src="http://116.206.196.146/uploads/2018-09/dghghdfdhf.jpg" alt="most viewed videos">
                  
                    <a href="http://www.indonesianfilmcenter.com/new_web/filmnews" class="hover-posts">
                        <span><i class="fa fa-play"></i>Lihat</span>
                    </a>
                </div>
                <div class="video-box-content">
                    <h6 style="margin-top: -10px;">
                        <b> <a href='https://celebrity.okezone.com' target='_blank' style='text-decoration:underline;color:#007eff;'>celebrity.okezone.com</a>                                   </b>
                               </h6>
                               <h3> <p><span style="font-size: 18px; line-height: 115%; font-family: 'arial black', 'avant garde'; color: black; background-repeat: initial; background-color: white;">Main Film Horor, <a href="http://www.indonesianfilmcenter.com/profil/index/director/18974" target="_blank" rel="noopener">Amanda Manopo</a> Merasa Keluar dari Zona Nyaman</span></p></h3>
                               <small >17 September 2018</small>
                   
                </div>
            </div>
                    </div>

            </div><!-- end carousel -->
        </div>
    </div>
</section>

<section style='background:green; border:solid 1px green;' id="movies">
    <div class="row" style="background:white; margin-top:-12px;">
         <div class="large-12  columns">
              <a href="https://www.biznetgio.com/"  alt="biznetgio" title="biznetgio"><img src="http://www.indonesianfilmcenter.com/images/banner-biznet.jpg" alt="biznetgio" title="biznetgio"></a>
        </div>
       
    </div>
</section>