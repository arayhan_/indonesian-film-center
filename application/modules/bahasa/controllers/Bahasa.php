<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bahasa extends CI_Controller {

	public function __construct() {
	 parent::__construct();
	}

	public function index() {
		show_404();
	}

	public function set_to($language) {
	 if(strtolower($language) === 'id') {
	  $lang = $language;
	 } else {
	  $lang = 'en';
	 }
	 set_cookie(
	  array(
	   'name' => 'lang_is',
	   'value' => $lang,
	   'expire'  => '8650',
	   'prefix'  => ''
	  )
	 );
	 
	 if($this->input->server('HTTP_REFERER')){
	  redirect($this->input->server('HTTP_REFERER'));
	 }
	}

}

/* End of file Bahasa.php */
/* Location: ./application/modules/bahasa/controllers/Bahasa.php */