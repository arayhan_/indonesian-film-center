<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filmblog extends CI_Controller {
  	public function __construct()
	{
		parent::__construct();
		$this->data['module']  = 'filmblog';
		if (@$this->input->cookie('lang_is')) {
			$this->data['lang'] =  $this->input->cookie('lang_is');
		} else {
			$this->data['lang'] =  'id';
		}
		$this->load->language('bahasa',$this->data['lang']);

	}
	public function index()
	{
		
	}

}

/* End of file Filmblog.php */
/* Location: ./application/modules/filmblog/controllers/Filmblog.php */