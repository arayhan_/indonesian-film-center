<header>
                <!-- Top -->
                <!-- <section id="top" class="topBar show-for-large">
                    <div class="row">
                        <div class="medium-6 columns">
                            <div class="socialLinks">
                                <a href="home-v1.html#"><i class="fa fa-facebook-f"></i></a>
                                <a href="home-v1.html#"><i class="fa fa-twitter"></i></a>
                                <a href="home-v1.html#"><i class="fa fa-google-plus"></i></a>
                                <a href="home-v1.html#"><i class="fa fa-instagram"></i></a>
                                <a href="home-v1.html#"><i class="fa fa-vimeo"></i></a>
                                <a href="home-v1.html#"><i class="fa fa-youtube"></i></a>
                            </div>
                        </div>
                        <div class="medium-6 columns">
                            <div class="top-button">
                               
                                <ul class="menu float-right">
                                    
                                    <li class="dropdown-login">
                                        <a class="loginReg" data-toggle="example-dropdown" href="home-v1.html#">login/Register</a>
                                        <div class="login-form">
                                            <h6 class="text-center">Great to have you back!</h6>
                                            <form method="post">
                                                <div class="input-group">
                                                    <span class="input-group-label"><i class="fa fa-user"></i></span>
                                                    <input class="input-group-field" type="text" placeholder="Enter username">
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-label"><i class="fa fa-lock"></i></span>
                                                    <input class="input-group-field" type="text" placeholder="Enter password">
                                                </div>
                                                <div class="checkbox">
                                                    <input id="check1" type="checkbox" name="check" value="check">
                                                    <label class="customLabel" for="check1">Remember me</label>
                                                </div>
                                                <input type="submit" name="submit" value="Login Now">
                                            </form>
                                            <p class="text-center">New here? <a class="newaccount" href="login-register.html">Create a new Account</a></p>
                                        </div>
                                    </li>
                                </ul>

                                 <div class="menu float-right">
                                     <?php echo anchor('url', '<img src="'.base_url('assets/images/lang/en.png').'" style="width:25px; " alt="England ">', ''); ?> <b>/</b>
                                     <?php echo anchor('url', '<img src="'.base_url('assets/images/lang/indonesia.png').'" style="width:25px; " alt="Indonesia">', ''); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section> --><!-- End Top -->
                <!--Navber-->
                <section id="navBar">
                    <nav class="sticky-container" data-sticky-container>
                        <div class="sticky topnav" data-sticky data-top-anchor="navBar" data-btm-anchor="footer-bottom:bottom" data-margin-top="0" data-margin-bottom="0" style="width: 100%; background: #fff;" data-sticky-on="large">
                            <div class="row">
                                <div class="large-12 columns">
                                <div class="title-bar" data-responsive-toggle="beNav" data-hide-for="large">
                                    <button class="menu-icon dark" type="button" data-toggle="offCanvas-responsive"></button>
                                    <div class="title-bar-title"><img src="<?php echo base_url('assets/') ?>images/idfc-logo.png" alt="logo"></div>
                                </div>

                                <div class="top-bar show-for-large" id="beNav" style="width: 100%;">
                                    <div class="top-bar-left">
                                        <ul class="menu">
                                            <li class="menu-text">
                                                <a href="<?= base_url(); ?>"><img src="<?php echo base_url('assets/') ?>images/idfc-logo.png" alt="logo"></a>
                                            </li>
                                        </ul>
                                    </div>
                                   <!--  <div class="top-bar-right search-btn">
                                        <ul class="menu">
                                            <li class="search">
                                                <i class="fa fa-search"></i>
                                            </li>
                                        </ul>
                                    </div> -->
                                    <div class="top-bar-right">
                                        <ul class="menu vertical medium-horizontal" data-responsive-menu="drilldown medium-dropdown">
                                            <li><a href="<?= base_url(); ?>"><b>Home</b></a></li>
                                            <li><a href="<?= base_url('watchfilms'); ?>"><b>WatchFilm</b></a></li>
                                            <li><a href="<?= base_url('filminfo'); ?>"><b>FilmInfo</b></a></li>
                                            <li><a href="<?= base_url('filmarchive'); ?>"><b>FilmArchive</b></a></li>
                                            <li><a href="<?= base_url('filmnews'); ?>"><b>FilmNews</b></a></li>
                                            <li><a href="<?= base_url('filmblog'); ?>"><b>FilmBlog</b></a></li>
                                            <li>
                                                <form class="ini-cari" action="/action_page.php"">
                                                  <input style="float: left"class="ini-cari"  type="text" placeholder="Search.." name="search2">
                                                  <button  style="float: left; " type="submit"><i  style="font-size: 20px; " class="fa fa-search wa"></i></button>
                                                </form>
                                            </li>
                                            
                                            <li>
                                                 <?php echo anchor('bahasa/set_to/id', '<img src="'.base_url('assets/images/lang/indonesia.png').'" style="width:25px;" alt="Indonesia">', 'style="float:left;"'); ?>
                                                <?php echo anchor('bahasa/set_to/en', '<img src="'.base_url('assets/images/lang/en.png').'" style="width:25px; " alt="England ">', 'style="float:right;"'); ?>
                                            </li>
                                            <li><a href="<?= base_url(); ?>"><b><?php echo $this->lang->line('login') ?></b></a></li>
                                        </ul>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <!-- <div id="search-bar" class="clearfix search-bar-light">
                                <form method="post">
                                    <div class="search-input float-left">
                                        <input type="search" name="search" placeholder="Seach Here your video">
                                    </div>
                                    <div class="search-btn float-right text-right">
                                        <button class="button" name="search" type="submit">search now</button>
                                    </div>
                                </form>
                            </div>
                        </div> -->
                    </nav>
                </section>
            </header>