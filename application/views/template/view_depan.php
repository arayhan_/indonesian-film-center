<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if (@$title): ?>
        <title><?= $title ?> - IdFilmCenter</title>
    <?php else: ?>
        <title>IdFilmCenter</title>
    <?php endif ?>
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/app.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/app.bundle.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/theme.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/modal.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Hind:300,400">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/') ?>layerslider/css/layerslider.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/custom.css">
      <link href="https://vjs.zencdn.net/7.1.0/video-js.css" rel="stylesheet">
      <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
      <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
      <?php if (@$css): ?>
        <?php echo get_css($css) ?>
    <?php endif ?>
</head>
<body style="background-color: green;">
    <div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper="">
        <!--header-->
        <!-- MENU KECIL -->
        <div class="off-canvas position-left light-off-menu" data-off-canvas="" id="offCanvas-responsive">
            <div class="off-menu-close">
                <h3>Menu</h3><span data-toggle="offCanvas-responsive"><i class="fa fa-times"></i></span>
            </div>
            <ul class="vertical menu off-menu" data-responsive-menu="drilldown">
                <li><a href="<?= base_url(); ?>"><b>Home</b></a></li>
                <li><a href="<?= base_url('watchfilms'); ?>"><b>WatchFilm</b></a></li>
                <li><a href="<?= base_url('filminfo'); ?>"><b>FilmInfo</b></a></li>
                <li><a href="<?= base_url('filmarchive'); ?>"><b>FilmArchive</b></a></li>
                <li><a href="<?= base_url('filmnews'); ?>"><b>FilmNews</b></a></li>
                <li><a href="<?= base_url('filmblog'); ?>"><b>FilmBlog</b></a></li>
            </ul>
            <div class="responsive-search">
                <form method="post">
                    <div class="input-group">
                        <input class="input-group-field" placeholder="search Here" type="text">
                        <div class="input-group-button">
                            <button name="search" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
          <div class="top-button" style="padding: 20px;">
            <h6>Bahasa</h6>
             <?php echo anchor('url', '<img src="'.base_url('assets/images/lang/en.png').'" style="width:25px; " alt="England ">','background:transparant;'); ?> <b>/</b>
             <?php echo anchor('url', '<img src="'.base_url('assets/images/lang/indonesia.png').'" style="width:25px; " alt="Indonesia">', 'background:transparant;'); ?>
        </div>
            <div class="off-social">
                <h6>Get Socialize</h6><a href="home-v1.html#"><i class="fa fa-facebook"></i></a> <a href="home-v1.html#"><i class="fa fa-twitter"></i></a> <a href="home-v1.html#"><i class="fa fa-google-plus"></i></a> <a href="home-v1.html#"><i class="fa fa-instagram"></i></a> <a href="home-v1.html#"><i class="fa fa-vimeo"></i></a> <a href="home-v1.html#"><i class="fa fa-youtube"></i></a>
            </div>
            <div class="top-button">
                <ul class="menu">
                    <li class="dropdown-login">
                        <a href="login.html">login/Register</a>
                    </li>
                </ul>
            </div>
        </div><!-- END -->
        <div class="icon-bar">
          <a href="#" class="facebook"><i class="fa fa-facebook"></i></a> 
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a> 
          <a href="#" class="google"><i class="fa fa-google"></i></a> 
          <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
          <a href="#" class="youtube"><i class="fa fa-youtube"></i></a> 
        </div>

        <div class="off-canvas-content" data-off-canvas-content="">
            <!-- End Header -->
            <?php $this->load->view('template/header', FALSE); ?><!-- layerslider -->


            <?php
                if (@$home==true) {
                   $this->load->view('template/slider', FALSE);
               }else{
                    $this->load->view('template/banner', FALSE);
                }
                


                echo ' <div style="background: white;">';
                if (@$module && @$page) {
                    $this->load->view($module.'/'.$page, FALSE);
                }elseif (@$page) {
                    $this->load->view($page, FALSE);
                }else{
                    $this->load->view('home/home', FALSE);
                }

            ?>
            
        

            <!-- footer -->
           <section style="background-color: green; "><div class="row" >
               <div class="large-12" style="padding-top: -100px; ">
                    <footer>
                <div class="row" style="margin-bottom: -80px;">
                    <div  style="padding-left: 30px; margin-top: -90px; " class="large-12 columns">
                        <div class="widgetBox" style="border-bottom: solid 1px white;">
                            <div class="widgetTitle">
                               <img class="logo logo-light " alt="logo" src="http://www.indonesianfilmcenter.com/img/idfc-logo.png" style="width: 130px; border-right: 1px solid white; float: left; margin-right: 10px; margin-bottom: 20px;">
                            </div>
                            <h5 style="color: white">All About Indonesian Film</h5>
                            
                        </div>
                    </div>
                    <div  style="padding-left: 30px;" class="large-4 medium-6 columns">
                        <div class="widgetBox">
                            
                            <div class="tagcloud" style="color:white; font-size: 14px;">
                               Di sini, Anda dapat menonton film pendek Indonesia, berbagai jenis lainnya dari video di WatchFilm, anda juga bisa mencari info tentang film Indonesia favorit Anda di FilmInfo dan membelinya di FilmShop, dan kabar berita terakhir tentang sinema Indonesia.
                           </br>
                               <div style="margin-top: 20px;">
                                   <small style="float: left; padding-top: 20px; margin-right: 10px; color: white; font-size: 13px">Powered By</small>
                                   <img src="http://www.indonesianfilmcenter.com/img/biznet.png" width="150px;">
                               </div>
                            </div>
                        </div>
                    </div>
                    <div  style="padding-left: 30px;" class="large-3 medium-6 columns">
                        <div class="widgetBox">
                            
                            <div class="tagcloud" style="color: white;">
                               
                        <h4  class="text-white">Follow Us</h4>
                        <table width="100%">
                            <tbody style="background-color: #444444; border:solid 2px #444444;"><tr>

                                                                    <td>
                                        <a href="https://www.facebook.com/IdFilmCenter/" target="_blank" class="col-md-3 socround back-fb">
                                            <i class="fa fa-facebook-official eafo"></i>
                                        </a>
                                    </td>
                                                                    <td>
                                        <a href="https://twitter.com/idfilmcenter?lang=en" target="_blank" class="col-md-3 socround back-twitter">
                                            <i class="fa fa-twitter-square eafo"></i>
                                        </a>
                                    </td>
                                                                    <td>
                                        <a href="https://www.youtube.com/user/IdFilmCenter" target="_blank" class="col-md-3 socround back-youtube">
                                            <i class="  fa fa-youtube-play eafo"></i>
                                        </a>
                                    </td>
                                                                    <td>
                                        <a href="https://www.instagram.com/idfilmcenter" target="_blank" class="col-md-3 socround back-instagram">
                                            <i class="  fa fa-instagram eafo"></i>
                                        </a>
                                    </td>
                                                            </tr>
                        </tbody></table>
                    
                            </div>
                        </div>
                    </div>
                    <div  style="padding-left: 30px;" class="large-2 medium-6 columns">
                        <div class="widgetBox">
                            
                            <div class="tagcloud">
                             <h4 class="text-white">Navigation</h4>
                                    <a href="<?= base_url('') ?>" style="font-weight: 300 !important;">Home</a>
                                    <a href="<?= base_url('watchfilms') ?>" style="font-weight: 300 !important;">WatchFilm</a>
                                    <a href="<?= base_url('filminfo') ?>" style="font-weight: 300 !important;">FilmInfo</a>
                                    <a href="<?= base_url('filmarchive') ?>" style="font-weight: 300 !important;">FilmArchive</a>
                                    <a href="<?= base_url('filmnews') ?>" style="font-weight: 300 !important;">FilmNews</a>
                                    <a href="<?= base_url('filmblog') ?>" style="font-weight: 300 !important;">FilmBlog</a>
                            </div>
                        </div>
                    </div>
                    <div  style="padding-left: 30px;" class="large-3 medium-6 columns">
                        <div class="widgetBox">
                            
                            <div class="widgetContent">
                                <h4 class="text-white">Email Newsletters</h4>
                                <form data-abide="imixha-abide" novalidate="" method="post">
                                    <p>Subscribe to get exclusive videos</p>
                                    <div class="input">
                                        <input type="text" placeholder="Enter your full Name" required="">
                                        <span class="form-error">
                                            Yo, you had better fill this out, it's required.
                                        </span>
                                    </div>
                                    <div class="input">
                                        <input type="email" id="email" placeholder="Enter your email addres" required="">
                                        <span class="form-error">
                                          I'm required!
                                        </span>
                                    </div>
                                    <button class="button" type="submit" name='submit' value="Submit">Sign up Now</button>
                                    <small><center>or</center></small>
                                    <button class="button" type="submit" name='submit2' value="Donate">Donate</button>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div><a href="home-v1.html#" id="back-to-top" title="Back to top"><i class="fa fa-angle-double-up"></i></a>
            </footer><!-- footer -->
           </section>
                 </div>

           
               </div>
           </div>
        </div><!--end off canvas content-->
    </div><!--end off canvas wrapper inner-->
</div><!--end off canvas wrapper-->
<!-- script files -->
<script src="<?php echo base_url('assets/') ?>bower_components/jquery/dist/jquery.js"></script>
<script src="<?php echo base_url('assets/') ?>bower_components/what-input/what-input.js"></script>
<script src="<?php echo base_url('assets/') ?>bower_components/foundation-sites/dist/foundation.js"></script>
<script src="<?php echo base_url('assets/') ?>js/jquery.showmore.src.js" type="text/javascript"></script>

<script src="<?php echo base_url('assets/') ?>js/app.js"></script>
<script src="<?php echo base_url('assets/') ?>layerslider/js/greensock.js" type="text/javascript"></script>
<!-- LayerSlider script files -->
<script src="<?php echo base_url('assets/') ?>layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/') ?>layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/') ?>js/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/inewsticker.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/') ?>js/jquery.kyco.easyshare.js" type="text/javascript"></script>
  <script src="https://vjs.zencdn.net/7.1.0/video.js"></script>
<?php if (@$js): ?>
    <?php echo get_js($js) ?>
<?php endif ?>


<style type="text/css">
  .modal a.close-modal[class*="icon-"] {
    top: -10px;
    right: -10px;
    width: 20px;
    height: 20px;
    color: #fff;
    line-height: 1.25;
    text-align: center;
    text-decoration: none;
    text-indent: 0;
    background: #900;
    border: 2px solid #fff;
    -webkit-border-radius: 26px;
    -moz-border-radius: 26px;
    -o-border-radius: 26px;
    -ms-border-radius: 26px;
    -moz-box-shadow:    1px 1px 5px rgba(0,0,0,0.5);
    -webkit-box-shadow: 1px 1px 5px rgba(0,0,0,0.5);
    box-shadow:         1px 1px 5px rgba(0,0,0,0.5);
  }
</style>


</body>
</html>