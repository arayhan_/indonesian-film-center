var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	uglifycss = require('gulp-uglifycss'),
	sourcemaps = require('gulp-sourcemaps'),
	rename = require('gulp-rename');

var PATH_APP_ASSETS = './assets/';

var path = {
	app: {
		css: PATH_APP_ASSETS + 'css/',
        sass: PATH_APP_ASSETS + 'scss/'
	}
}
    
gulp.task('sass', function(){
    gulp.src(path.app.sass + 'app.scss')
        .pipe(sass())
        .pipe(sourcemaps.init())
        .pipe(autoprefixer('last 2 versions'))
        .pipe(rename('app.bundle.css'))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest(path.app.css));
});

gulp.task('serve', function() {
	gulp.watch(path.app.sass + '**/*.scss', ['sass']);
});